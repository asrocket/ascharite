function Ascharite() {
	this.sockjs = new SockJS("/aschariteEventEndpoint");
	this.stompClient = Stomp.over(this.sockjs);
	this.handlers = {};
	var ascharite = this;

	ascharite.stompClient.connect({}, function (frame) {
		ascharite.stompClient.subscribe('/topic/aschariteEventTopic', function (e) {
			ascharite.defaultMessageHandler(e);
		});

		ascharite.stompClient.subscribe('/user/topic/aschariteEventTopic', function (e) {
			ascharite.defaultMessageHandler(e);
		});

	});
}

Ascharite.prototype.sendEvent = function (type, data) {
	if (typeof(data) === 'string') {
		this.stompClient.send("/ascharite/" + type, {}, data);
		return
	}
	this.stompClient.send("/ascharite/" + type, {}, JSON.stringify(data));
};

Ascharite.prototype.sendEmptyEvent = function (type) {
	this.sendEvent(type, null)
};

Ascharite.prototype.logError = function (msg) {
	this.sendEvent("ascharite.log.error", msg)
};

Ascharite.prototype.addHandler = function (type, handler) {
	this.handlers[type] = handler;
};

Ascharite.prototype.commonHandler = function (id, handler) {
	var element = document.getElementById(id);
	if (element) {
		handler(element);
	} else {
		console.log("Element with id = " + id + " not found");
		this.logError("ascharite.log.error", "Element with id = " + id + " not found")
	}
};

Ascharite.prototype.defaultMessageHandler = function (e) {
	var event = JSON.parse(e.body);
	var handler = ascharite.handlers[event.type]
	if (handler) {
		handler(event.data, event.id);
	} else {
		console.log("Handler not found for event = " + event.type);
		ascharite.logError("Handler not found for event = " + event.type);
	}
};

var ascharite = new Ascharite();

function sendEvent(type, data) {
	ascharite.sendEvent(type, data);
}

ascharite.addHandler('consolelog', consoleLogHandler);
ascharite.addHandler('innerHtml', innerHtmlHandler);
ascharite.addHandler('innerText', innerTextHandler);
ascharite.addHandler('value', valueHandler);

function consoleLogHandler(data, id) {
	console.log(data);
}

function innerHtmlHandler(data, id) {
	ascharite.commonHandler(id, function (element) {
		element.innerHTML = data;
	})
}

function innerTextHandler(data, id) {
	ascharite.commonHandler(id, function (element) {
		element.innerText = data;
	})
}

function valueHandler(data, id) {
	ascharite.commonHandler(id, function (element) {
		element.value = data;
	})
}