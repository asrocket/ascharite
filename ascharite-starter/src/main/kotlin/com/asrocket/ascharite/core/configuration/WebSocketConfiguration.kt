package com.asrocket.ascharite.core.configuration

import com.asrocket.ascharite.core.event.EventSender
import com.asrocket.ascharite.core.annotation.SessionId
import org.springframework.context.annotation.Bean
import org.springframework.core.MethodParameter
import org.springframework.http.server.ServerHttpRequest
import org.springframework.http.server.ServerHttpResponse
import org.springframework.http.server.ServletServerHttpRequest
import org.springframework.messaging.Message
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.server.HandshakeInterceptor
import java.lang.Exception

@EnableWebSocketMessageBroker
class WebSocketConfiguration : WebSocketMessageBrokerConfigurer {

    @Bean
    fun eventSender(simpMessagingTemplate: SimpMessagingTemplate) = EventSender(simpMessagingTemplate)

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("/aschariteEventEndpoint")
                .addInterceptors(CustomHandshakeInterceptor())
                .withSockJS()
    }

    override fun configureMessageBroker(registry: MessageBrokerRegistry) {
        registry.enableSimpleBroker("/topic/")
        registry.setApplicationDestinationPrefixes("/ascharite")
    }

    override fun addArgumentResolvers(argumentResolvers: MutableList<HandlerMethodArgumentResolver>) {
        argumentResolvers.add(SessionIdArgumentResolver())
    }

    class SessionIdArgumentResolver : HandlerMethodArgumentResolver {

        override fun supportsParameter(parameter: MethodParameter) = parameter.hasParameterAnnotation(SessionId::class.java)

        override fun resolveArgument(parameter: MethodParameter, message: Message<*>) = SimpMessageHeaderAccessor.getSessionId(message.headers)

    }

    class CustomHandshakeInterceptor : HandshakeInterceptor {

        override fun beforeHandshake(request: ServerHttpRequest, response: ServerHttpResponse, wsHandler: WebSocketHandler, attributes: MutableMap<String, Any>): Boolean {
            if (request is ServletServerHttpRequest) {
                val session = request.servletRequest.session
                attributes["sessionId"] = session.id
            }
            return true
        }

        override fun afterHandshake(request: ServerHttpRequest, response: ServerHttpResponse, wsHandler: WebSocketHandler, exception: Exception?) {}
    }
}