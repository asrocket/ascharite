package com.asrocket.ascharite.core.annotation

import org.springframework.messaging.simp.annotation.SendToUser

@Retention
@Target(AnnotationTarget.FUNCTION)
@SendToUser("/topic/aschariteEventTopic")
annotation class SendToUser