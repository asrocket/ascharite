package com.asrocket.ascharite.core.annotation

import org.springframework.messaging.handler.annotation.SendTo

@Retention
@Target(AnnotationTarget.FUNCTION)
@SendTo("/topic/aschariteEventTopic")
annotation class SendTo