package com.asrocket.ascharite.core.html

abstract class HtmlTag(name: String) : Tag(name) {
    var id: String
        get() = attributes["id"] ?: ""
        set(value) {
            attributes["id"] = value
        }

    var `class`: String
        get() = attributes["class"] ?: ""
        set(value) {
            attributes["class"] = value
        }

    fun head(init: Head.() -> Unit) = initTag(Head(), init)
    fun title(init: Title.() -> Unit) = initTag(Title(), init)
    fun base(init: Base.() -> Unit) = initTag(Base(), init)
    fun link(init: Link.() -> Unit) = initTag(Link(), init)
    fun meta(init: Meta.() -> Unit) = initTag(Meta(), init)
    fun style(init: Style.() -> Unit) = initTag(Style(), init)
    fun body(init: Body.() -> Unit) = initTag(Body(), init)
    fun article(init: Article.() -> Unit) = initTag(Article(), init)
    fun section(init: Section.() -> Unit) = initTag(Section(), init)
    fun nav(init: Nav.() -> Unit) = initTag(Nav(), init)
    fun aside(init: Aside.() -> Unit) = initTag(Aside(), init)
    fun h1(init: H1.() -> Unit) = initTag(H1(), init)
    fun h2(init: H2.() -> Unit) = initTag(H2(), init)
    fun h3(init: H3.() -> Unit) = initTag(H3(), init)
    fun h4(init: H4.() -> Unit) = initTag(H4(), init)
    fun h5(init: H5.() -> Unit) = initTag(H5(), init)
    fun h6(init: H6.() -> Unit) = initTag(H6(), init)
    fun header(init: Header.() -> Unit) = initTag(Header(), init)
    fun footer(init: Footer.() -> Unit) = initTag(Footer(), init)
    fun address(init: Address.() -> Unit) = initTag(Address(), init)
    fun p(init: P.() -> Unit) = initTag(P(), init)
    fun pre(init: Pre.() -> Unit) = initTag(Pre(), init)
    fun blockquote(init: Blockquote.() -> Unit) = initTag(Blockquote(), init)
    fun ol(init: Ol.() -> Unit) = initTag(Ol(), init)
    fun ul(init: Ul.() -> Unit) = initTag(Ul(), init)
    fun li(init: Li.() -> Unit) = initTag(Li(), init)
    fun dl(init: Dl.() -> Unit) = initTag(Dl(), init)
    fun dt(init: Dt.() -> Unit) = initTag(Dt(), init)
    fun dd(init: Dd.() -> Unit) = initTag(Dd(), init)
    fun figure(init: Figure.() -> Unit) = initTag(Figure(), init)
    fun figcaption(init: Figcaption.() -> Unit) = initTag(Figcaption(), init)
    fun div(init: Div.() -> Unit) = initTag(Div(), init)
    fun main(init: Main.() -> Unit) = initTag(Main(), init)
    fun hr(init: Hr.() -> Unit) = initTag(Hr(), init)
    fun a(init: A.() -> Unit) = initTag(A(), init)
    fun em(init: Em.() -> Unit) = initTag(Em(), init)
    fun strong(init: Strong.() -> Unit) = initTag(Strong(), init)
    fun cite(init: Cite.() -> Unit) = initTag(Cite(), init)
    fun q(init: Q.() -> Unit) = initTag(Q(), init)
    fun dfn(init: Dfn.() -> Unit) = initTag(Dfn(), init)
    fun abbr(init: Abbr.() -> Unit) = initTag(Abbr(), init)
    fun data(init: Data.() -> Unit) = initTag(Data(), init)
    fun time(init: Time.() -> Unit) = initTag(Time(), init)
    fun code(init: Code.() -> Unit) = initTag(Code(), init)
    fun `var`(init: Var.() -> Unit) = initTag(Var(), init)
    fun samp(init: Samp.() -> Unit) = initTag(Samp(), init)
    fun kbd(init: Kbd.() -> Unit) = initTag(Kbd(), init)
    fun mark(init: Mark.() -> Unit) = initTag(Mark(), init)
    fun ruby(init: Ruby.() -> Unit) = initTag(Ruby(), init)
    fun rb(init: Rb.() -> Unit) = initTag(Rb(), init)
    fun rt(init: Rt.() -> Unit) = initTag(Rt(), init)
    fun rp(init: Rp.() -> Unit) = initTag(Rp(), init)
    fun rtc(init: Rtc.() -> Unit) = initTag(Rtc(), init)
    fun bdi(init: Bdi.() -> Unit) = initTag(Bdi(), init)
    fun bdo(init: Bdo.() -> Unit) = initTag(Bdo(), init)
    fun span(init: Span.() -> Unit) = initTag(Span(), init)
    fun br(init: Br.() -> Unit) = initTag(Br(), init)
    fun wbr(init: Wbr.() -> Unit) = initTag(Wbr(), init)
    fun small(init: Small.() -> Unit) = initTag(Small(), init)
    fun i(init: I.() -> Unit) = initTag(I(), init)
    fun b(init: B.() -> Unit) = initTag(B(), init)
    fun u(init: U.() -> Unit) = initTag(U(), init)
    fun s(init: S.() -> Unit) = initTag(S(), init)
    fun sub(init: Sub.() -> Unit) = initTag(Sub(), init)
    fun sup(init: Sup.() -> Unit) = initTag(Sup(), init)
    fun ins(init: Ins.() -> Unit) = initTag(Ins(), init)
    fun del(init: Del.() -> Unit) = initTag(Del(), init)
    fun img(init: Img.() -> Unit) = initTag(Img(), init)
    fun embed(init: Embed.() -> Unit) = initTag(Embed(), init)
    fun `object`(init: Object.() -> Unit) = initTag(Object(), init)
    fun param(init: Param.() -> Unit) = initTag(Param(), init)
    fun video(init: Video.() -> Unit) = initTag(Video(), init)
    fun audio(init: Audio.() -> Unit) = initTag(Audio(), init)
    fun source(init: Source.() -> Unit) = initTag(Source(), init)
    fun track(init: Track.() -> Unit) = initTag(Track(), init)
    fun map(init: Map.() -> Unit) = initTag(Map(), init)
    fun area(init: Area.() -> Unit) = initTag(Area(), init)
    fun iframe(init: Iframe.() -> Unit) = initTag(Iframe(), init)
    fun table(init: Table.() -> Unit) = initTag(Table(), init)
    fun tr(init: Tr.() -> Unit) = initTag(Tr(), init)
    fun td(init: Td.() -> Unit) = initTag(Td(), init)
    fun th(init: Th.() -> Unit) = initTag(Th(), init)
    fun caption(init: Caption.() -> Unit) = initTag(Caption(), init)
    fun tbody(init: Tbody.() -> Unit) = initTag(Tbody(), init)
    fun thead(init: Thead.() -> Unit) = initTag(Thead(), init)
    fun tfoot(init: Tfoot.() -> Unit) = initTag(Tfoot(), init)
    fun colgroup(init: Colgroup.() -> Unit) = initTag(Colgroup(), init)
    fun col(init: Col.() -> Unit) = initTag(Col(), init)
    fun form(init: Form.() -> Unit) = initTag(Form(), init)
    fun input(init: Input.() -> Unit) = initTag(Input(), init)
    fun textarea(init: Textarea.() -> Unit) = initTag(Textarea(), init)
    fun select(init: Select.() -> Unit) = initTag(Select(), init)
    fun option(init: Option.() -> Unit) = initTag(Option(), init)
    fun optgroup(init: Optgroup.() -> Unit) = initTag(Optgroup(), init)
    fun datalist(init: Datalist.() -> Unit) = initTag(Datalist(), init)
    fun label(init: Label.() -> Unit) = initTag(Label(), init)
    fun fieldset(init: Fieldset.() -> Unit) = initTag(Fieldset(), init)
    fun legend(init: Legend.() -> Unit) = initTag(Legend(), init)
    fun button(init: Button.() -> Unit) = initTag(Button(), init)
    fun output(init: Output.() -> Unit) = initTag(Output(), init)
    fun progress(init: Progress.() -> Unit) = initTag(Progress(), init)
    fun meter(init: Meter.() -> Unit) = initTag(Meter(), init)
    fun keygen(init: Keygen.() -> Unit) = initTag(Keygen(), init)
    fun script(init: Script.() -> Unit) = initTag(Script(), init)
    fun noscript(init: Noscript.() -> Unit) = initTag(Noscript(), init)
    fun template(init: Template.() -> Unit) = initTag(Template(), init)
    fun canvas(init: Canvas.() -> Unit) = initTag(Canvas(), init)
}

//The Root Element
class Html : HtmlTag("html")

fun html(init: Html.() -> Unit): Html {
    val html = Html()
    html.init()
    return html
}


//Metadata
class Head : HtmlTag("head")

fun head(init: Head.() -> Unit): Head {
    val head = Head()
    head.init()
    return head
}

class Title : HtmlTag("title")

fun title(init: Title.() -> Unit): Title {
    val title = Title()
    title.init()
    return title
}

class Base : HtmlTag("base")

fun base(init: Base.() -> Unit): Base {
    val base = Base()
    base.init()
    return base
}

class Link : HtmlTag("link") {
    override fun render(builder: StringBuilder, indent: String, prettify: Boolean) {
        if (prettify) {
            builder.append("$indent<$name${renderAttributes()}>\n")
            for (c in children) {
                c.render(builder, "  $indent", prettify)
            }
        } else {
            builder.append("$indent<$name${renderAttributes()}>")
            for (c in children) {
                c.render(builder, "$indent", prettify)
            }
        }
    }
}

fun link(init: Link.() -> Unit): Link {
    val link = Link()
    link.init()
    return link
}

class Meta : HtmlTag("meta")

fun meta(init: Meta.() -> Unit): Meta {
    val meta = Meta()
    meta.init()
    return meta
}

class Style : HtmlTag("style")

fun style(init: Style.() -> Unit): Style {
    val style = Style()
    style.init()
    return style
}


//Sections
class Body : HtmlTag("body")

fun body(init: Body.() -> Unit): Body {
    val body = Body()
    body.init()
    return body
}

class Article : HtmlTag("article")

fun article(init: Article.() -> Unit): Article {
    val article = Article()
    article.init()
    return article
}

class Section : HtmlTag("section")

fun section(init: Section.() -> Unit): Section {
    val section = Section()
    section.init()
    return section
}

class Nav : HtmlTag("nav")

fun nav(init: Nav.() -> Unit): Nav {
    val nav = Nav()
    nav.init()
    return nav
}

class Aside : HtmlTag("aside")

fun aside(init: Aside.() -> Unit): Aside {
    val aside = Aside()
    aside.init()
    return aside
}

class H1 : HtmlTag("h1")

fun h1(init: H1.() -> Unit): H1 {
    val h1 = H1()
    h1.init()
    return h1
}

class H2 : HtmlTag("h2")

fun h2(init: H2.() -> Unit): H2 {
    val h2 = H2()
    h2.init()
    return h2
}

class H3 : HtmlTag("h3")

fun h3(init: H3.() -> Unit): H3 {
    val h3 = H3()
    h3.init()
    return h3
}

class H4 : HtmlTag("h4")

fun h4(init: H4.() -> Unit): H4 {
    val h4 = H4()
    h4.init()
    return h4
}

class H5 : HtmlTag("h5")

fun h5(init: H5.() -> Unit): H5 {
    val h5 = H5()
    h5.init()
    return h5
}

class H6 : HtmlTag("h6")

fun h6(init: H6.() -> Unit): H6 {
    val h6 = H6()
    h6.init()
    return h6
}

class Header : HtmlTag("header")

fun header(init: Header.() -> Unit): Header {
    val header = Header()
    header.init()
    return header
}

class Footer : HtmlTag("footer")

fun footer(init: Footer.() -> Unit): Footer {
    val footer = Footer()
    footer.init()
    return footer
}

class Address : HtmlTag("address")

fun address(init: Address.() -> Unit): Address {
    val address = Address()
    address.init()
    return address
}


//Grouping
class P : HtmlTag("p")

fun p(init: P.() -> Unit): P {
    val p = P()
    p.init()
    return p
}

class Pre : HtmlTag("pre")

fun pre(init: Pre.() -> Unit): Pre {
    val pre = Pre()
    pre.init()
    return pre
}

class Blockquote : HtmlTag("blockquote")

fun blockquote(init: Blockquote.() -> Unit): Blockquote {
    val blockquote = Blockquote()
    blockquote.init()
    return blockquote
}

class Ol : HtmlTag("ol")

fun ol(init: Ol.() -> Unit): Ol {
    val ol = Ol()
    ol.init()
    return ol
}

class Ul : HtmlTag("ul")

fun ul(init: Ul.() -> Unit): Ul {
    val ul = Ul()
    ul.init()
    return ul
}

class Li : HtmlTag("li")

fun li(init: Li.() -> Unit): Li {
    val li = Li()
    li.init()
    return li
}

class Dl : HtmlTag("dl")

fun dl(init: Dl.() -> Unit): Dl {
    val dl = Dl()
    dl.init()
    return dl
}

class Dt : HtmlTag("dt")

fun dt(init: Dt.() -> Unit): Dt {
    val dt = Dt()
    dt.init()
    return dt
}

class Dd : HtmlTag("dd")

fun dd(init: Dd.() -> Unit): Dd {
    val dd = Dd()
    dd.init()
    return dd
}

class Figure : HtmlTag("figure")

fun figure(init: Figure.() -> Unit): Figure {
    val figure = Figure()
    figure.init()
    return figure
}

class Figcaption : HtmlTag("figcaption")

fun figcaption(init: Figcaption.() -> Unit): Figcaption {
    val figcaption = Figcaption()
    figcaption.init()
    return figcaption
}

class Div : HtmlTag("div")

fun div(init: Div.() -> Unit): Div {
    val div = Div()
    div.init()
    return div
}

class Main : HtmlTag("main")

fun main(init: Main.() -> Unit): Main {
    val main = Main()
    main.init()
    return main
}

class Hr : HtmlTag("hr")

fun hr(init: Hr.() -> Unit): Hr {
    val hr = Hr()
    hr.init()
    return hr
}


//Text
class A : HtmlTag("a") {
    var href: String? = null
        set(value) {
            if (value == null) return
            attributes["href"] = value
        }
}

fun a(init: A.() -> Unit): A {
    val a = A()
    a.init()
    return a
}

class Em : HtmlTag("em")

fun em(init: Em.() -> Unit): Em {
    val em = Em()
    em.init()
    return em
}

class Strong : HtmlTag("strong")

fun strong(init: Strong.() -> Unit): Strong {
    val strong = Strong()
    strong.init()
    return strong
}

class Cite : HtmlTag("cite")

fun cite(init: Cite.() -> Unit): Cite {
    val cite = Cite()
    cite.init()
    return cite
}

class Q : HtmlTag("q")

fun q(init: Q.() -> Unit): Q {
    val q = Q()
    q.init()
    return q
}

class Dfn : HtmlTag("dfn")

fun dfn(init: Dfn.() -> Unit): Dfn {
    val dfn = Dfn()
    dfn.init()
    return dfn
}

class Abbr : HtmlTag("abbr")

fun abbr(init: Abbr.() -> Unit): Abbr {
    val abbr = Abbr()
    abbr.init()
    return abbr
}

class Data : HtmlTag("data")

fun data(init: Data.() -> Unit): Data {
    val data = Data()
    data.init()
    return data
}

class Time : HtmlTag("time")

fun time(init: Time.() -> Unit): Time {
    val time = Time()
    time.init()
    return time
}

class Code : HtmlTag("code")

fun code(init: Code.() -> Unit): Code {
    val code = Code()
    code.init()
    return code
}

class Var : HtmlTag("var")

fun `var`(init: Var.() -> Unit): Var {
    val `var` = Var()
    `var`.init()
    return `var`
}

class Samp : HtmlTag("samp")

fun samp(init: Samp.() -> Unit): Samp {
    val samp = Samp()
    samp.init()
    return samp
}

class Kbd : HtmlTag("kbd")

fun kbd(init: Kbd.() -> Unit): Kbd {
    val kbd = Kbd()
    kbd.init()
    return kbd
}

class Mark : HtmlTag("mark")

fun mark(init: Mark.() -> Unit): Mark {
    val mark = Mark()
    mark.init()
    return mark
}

class Ruby : HtmlTag("ruby")

fun ruby(init: Ruby.() -> Unit): Ruby {
    val ruby = Ruby()
    ruby.init()
    return ruby
}

class Rb : HtmlTag("rb")

fun rb(init: Rb.() -> Unit): Rb {
    val rb = Rb()
    rb.init()
    return rb
}

class Rt : HtmlTag("rt")

fun rt(init: Rt.() -> Unit): Rt {
    val rt = Rt()
    rt.init()
    return rt
}

class Rp : HtmlTag("rp")

fun rp(init: Rp.() -> Unit): Rp {
    val rp = Rp()
    rp.init()
    return rp
}

class Rtc : HtmlTag("rtc")

fun rtc(init: Rtc.() -> Unit): Rtc {
    val rtc = Rtc()
    rtc.init()
    return rtc
}

class Bdi : HtmlTag("bdi")

fun bdi(init: Bdi.() -> Unit): Bdi {
    val bdi = Bdi()
    bdi.init()
    return bdi
}

class Bdo : HtmlTag("bdo")

fun bdo(init: Bdo.() -> Unit): Bdo {
    val bdo = Bdo()
    bdo.init()
    return bdo
}

class Span : HtmlTag("span")

fun span(init: Span.() -> Unit): Span {
    val span = Span()
    span.init()
    return span
}

class Br : HtmlTag("br")

fun br(init: Br.() -> Unit): Br {
    val br = Br()
    br.init()
    return br
}

class Wbr : HtmlTag("wbr")

fun wbr(init: Wbr.() -> Unit): Wbr {
    val wbr = Wbr()
    wbr.init()
    return wbr
}

class Small : HtmlTag("small")

fun small(init: Small.() -> Unit): Small {
    val small = Small()
    small.init()
    return small
}

class I : HtmlTag("i")

fun i(init: I.() -> Unit): I {
    val i = I()
    i.init()
    return i
}

class B : HtmlTag("b")

fun b(init: B.() -> Unit): B {
    val b = B()
    b.init()
    return b
}

class U : HtmlTag("u")

fun u(init: U.() -> Unit): U {
    val u = U()
    u.init()
    return u
}

class S : HtmlTag("s")

fun s(init: S.() -> Unit): S {
    val s = S()
    s.init()
    return s
}

class Sub : HtmlTag("sub")

fun sub(init: Sub.() -> Unit): Sub {
    val sub = Sub()
    sub.init()
    return sub
}

class Sup : HtmlTag("sup")

fun sup(init: Sup.() -> Unit): Sup {
    val sup = Sup()
    sup.init()
    return sup
}


//Edits
class Ins : HtmlTag("ins")

fun ins(init: Ins.() -> Unit): Ins {
    val ins = Ins()
    ins.init()
    return ins
}

class Del : HtmlTag("del")

fun del(init: Del.() -> Unit): Del {
    val del = Del()
    del.init()
    return del
}


//Embedded Content
class Img : HtmlTag("img")

fun img(init: Img.() -> Unit): Img {
    val img = Img()
    img.init()
    return img
}

class Embed : HtmlTag("embed")

fun embed(init: Embed.() -> Unit): Embed {
    val embed = Embed()
    embed.init()
    return embed
}

class Object : HtmlTag("object")

fun `object`(init: Object.() -> Unit): Object {
    val `object` = Object()
    `object`.init()
    return `object`
}

class Param : HtmlTag("param")

fun param(init: Param.() -> Unit): Param {
    val param = Param()
    param.init()
    return param
}

class Video : HtmlTag("video")

fun video(init: Video.() -> Unit): Video {
    val video = Video()
    video.init()
    return video
}

class Audio : HtmlTag("audio")

fun audio(init: Audio.() -> Unit): Audio {
    val audio = Audio()
    audio.init()
    return audio
}

class Source : HtmlTag("source")

fun source(init: Source.() -> Unit): Source {
    val source = Source()
    source.init()
    return source
}

class Track : HtmlTag("track")

fun track(init: Track.() -> Unit): Track {
    val track = Track()
    track.init()
    return track
}

class Map : HtmlTag("map")

fun map(init: Map.() -> Unit): Map {
    val map = Map()
    map.init()
    return map
}

class Area : HtmlTag("area")

fun area(init: Area.() -> Unit): Area {
    val area = Area()
    area.init()
    return area
}

class Iframe : HtmlTag("iframe")

fun iframe(init: Iframe.() -> Unit): Iframe {
    val iframe = Iframe()
    iframe.init()
    return iframe
}


//Tables
class Table : HtmlTag("table")

fun table(init: Table.() -> Unit): Table {
    val table = Table()
    table.init()
    return table
}

class Tr : HtmlTag("tr")

fun tr(init: Tr.() -> Unit): Tr {
    val tr = Tr()
    tr.init()
    return tr
}

class Td : HtmlTag("td")

fun td(init: Td.() -> Unit): Td {
    val td = Td()
    td.init()
    return td
}

class Th : HtmlTag("th")

fun th(init: Th.() -> Unit): Th {
    val th = Th()
    th.init()
    return th
}

class Caption : HtmlTag("caption")

fun caption(init: Caption.() -> Unit): Caption {
    val caption = Caption()
    caption.init()
    return caption
}

class Tbody : HtmlTag("tbody")

fun tbody(init: Tbody.() -> Unit): Tbody {
    val tbody = Tbody()
    tbody.init()
    return tbody
}

class Thead : HtmlTag("thead")

fun thead(init: Thead.() -> Unit): Thead {
    val thead = Thead()
    thead.init()
    return thead
}

class Tfoot : HtmlTag("tfoot")

fun tfoot(init: Tfoot.() -> Unit): Tfoot {
    val tfoot = Tfoot()
    tfoot.init()
    return tfoot
}

class Colgroup : HtmlTag("colgroup")

fun colgroup(init: Colgroup.() -> Unit): Colgroup {
    val colgroup = Colgroup()
    colgroup.init()
    return colgroup
}

class Col : HtmlTag("col")

fun col(init: Col.() -> Unit): Col {
    val col = Col()
    col.init()
    return col
}


//Forms
class Form : HtmlTag("form")

fun form(init: Form.() -> Unit): Form {
    val form = Form()
    form.init()
    return form
}

class Input : HtmlTag("input")

fun input(init: Input.() -> Unit): Input {
    val input = Input()
    input.init()
    return input
}

class Textarea : HtmlTag("textarea")

fun textarea(init: Textarea.() -> Unit): Textarea {
    val textarea = Textarea()
    textarea.init()
    return textarea
}

class Select : HtmlTag("select")

fun select(init: Select.() -> Unit): Select {
    val select = Select()
    select.init()
    return select
}

class Option : HtmlTag("option")

fun option(init: Option.() -> Unit): Option {
    val option = Option()
    option.init()
    return option
}

class Optgroup : HtmlTag("optgroup")

fun optgroup(init: Optgroup.() -> Unit): Optgroup {
    val optgroup = Optgroup()
    optgroup.init()
    return optgroup
}

class Datalist : HtmlTag("datalist")

fun datalist(init: Datalist.() -> Unit): Datalist {
    val datalist = Datalist()
    datalist.init()
    return datalist
}

class Label : HtmlTag("label")

fun label(init: Label.() -> Unit): Label {
    val label = Label()
    label.init()
    return label
}

class Fieldset : HtmlTag("fieldset")

fun fieldset(init: Fieldset.() -> Unit): Fieldset {
    val fieldset = Fieldset()
    fieldset.init()
    return fieldset
}

class Legend : HtmlTag("legend")

fun legend(init: Legend.() -> Unit): Legend {
    val legend = Legend()
    legend.init()
    return legend
}

class Button : HtmlTag("button")

fun button(init: Button.() -> Unit): Button {
    val button = Button()
    button.init()
    return button
}

class Output : HtmlTag("output")

fun output(init: Output.() -> Unit): Output {
    val output = Output()
    output.init()
    return output
}

class Progress : HtmlTag("progress")

fun progress(init: Progress.() -> Unit): Progress {
    val progress = Progress()
    progress.init()
    return progress
}

class Meter : HtmlTag("meter")

fun meter(init: Meter.() -> Unit): Meter {
    val meter = Meter()
    meter.init()
    return meter
}

class Keygen : HtmlTag("keygen")

fun keygen(init: Keygen.() -> Unit): Keygen {
    val keygen = Keygen()
    keygen.init()
    return keygen
}


//Scripting
class Script : HtmlTag("script") {
    var src: String = ""
        set(value) {
            attributes["src"] = value
        }
}

fun script(init: Script.() -> Unit): Script {
    val script = Script()
    script.init()
    return script
}

class Noscript : HtmlTag("noscript")

fun noscript(init: Noscript.() -> Unit): Noscript {
    val noscript = Noscript()
    noscript.init()
    return noscript
}

class Template : HtmlTag("template")

fun template(init: Template.() -> Unit): Template {
    val template = Template()
    template.init()
    return template
}

class Canvas : HtmlTag("canvas")

fun canvas(init: Canvas.() -> Unit): Canvas {
    val canvas = Canvas()
    canvas.init()
    return canvas
}
