package com.asrocket.ascharite.core.js

data class JsFunction(val text: String)

fun sendEvent(type: String) = "sendEvent('$type')"

fun sendEvent(type: String, data: String) = "sendEvent('$type', '$data')"

fun sendEvent(type: String, data: JsFunction) = "sendEvent('$type', ${data.text})"

fun sendEvent(type: String, data: Number) = "sendEvent('$type', $data)"

fun sendEvent(type: String, data: Any) = "sendEvent('$type', $data)"

fun addEventListener(id: String, event: String, func: String) = "document.getElementById('$id').addEventListener('$event', function () {$func});"