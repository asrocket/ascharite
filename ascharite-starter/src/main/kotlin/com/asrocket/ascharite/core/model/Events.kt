package com.asrocket.ascharite.core.model

import com.asrocket.ascharite.core.html.HtmlTag

fun log(msg: String) = Event("consolelog", msg)

fun innerHtml(id: String, htmlTag: HtmlTag) = ElementEvent("innerHtml", id, htmlTag.toString())

fun innerHtml(id: String, html: () -> HtmlTag) = ElementEvent("innerHtml", id, html.invoke().toString())

fun innerText(id: String, text: String) = ElementEvent("innerText", id, text)