package com.asrocket.ascharite.core.event

import com.asrocket.ascharite.core.model.Event
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.messaging.simp.SimpMessageType
import org.springframework.messaging.simp.SimpMessagingTemplate

class EventSender(private val simpMessagingTemplate: SimpMessagingTemplate) {

    fun send(data: Event) {
        simpMessagingTemplate.convertAndSend("/topic/aschariteEventTopic", data)
    }

    fun sendToUser(username: String, data: Event) {
        simpMessagingTemplate.convertAndSendToUser(username, "/topic/aschariteEventTopic", data)
    }

    fun sendToSession(sessionId: String, data: Event) {
        val headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE)
        headerAccessor.sessionId = sessionId
        headerAccessor.setLeaveMutable(true)
        simpMessagingTemplate.convertAndSendToUser(sessionId, "/topic/aschariteEventTopic", data, headerAccessor.messageHeaders)
    }

    fun forAll() = EventBuilder { send(it) }

    fun forUser(username: String) = EventBuilder { sendToUser(username, it) }

    fun forSession(sessionId: String) = EventBuilder { sendToSession(sessionId, it) }
}

