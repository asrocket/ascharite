package com.asrocket.ascharite.core

import kotlin.reflect.KFunction

interface AschariteHandlerMapping {
    fun getHandler(event: String): KFunction<*>?
}