package com.asrocket.ascharite.core.event

import com.asrocket.ascharite.core.html.HtmlTag
import com.asrocket.ascharite.core.model.ElementEvent
import com.asrocket.ascharite.core.model.Event

class EventBuilder(private val sender: (event: Event) -> Unit) {

    fun log(text: String) {
        val event = Event("consolelog", text)
        sender.invoke(event)
    }

    fun element(id: String) = ElementEventBuilder(id, sender)
}

class ElementEventBuilder(private val elementId: String, private val sender: (event: Event) -> Unit) {

    var innerHtml: HtmlTag? = null
        set(value) {
            if (value != null) {
                val elementEvent = ElementEvent("innerHtml", elementId, value.toString())
                sender.invoke(elementEvent)
            }
        }

    var innerText: String? = null
        set(value) {
            if (value != null) {
                val elementEvent = ElementEvent("innerText", elementId, value)
                sender.invoke(elementEvent)
            }
        }

    var value: Any? = null
        set(value) {
            if (value != null) {
                val elementEvent = ElementEvent("value", elementId, value)
                sender.invoke(elementEvent)
            }
        }
}