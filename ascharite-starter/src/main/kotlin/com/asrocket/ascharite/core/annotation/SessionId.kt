package com.asrocket.ascharite.core.annotation

@Retention
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class SessionId