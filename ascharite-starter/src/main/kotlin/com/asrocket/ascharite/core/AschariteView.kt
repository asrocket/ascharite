package com.asrocket.ascharite.core

import com.asrocket.ascharite.core.html.*
import org.springframework.web.servlet.View
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AschariteView : View {
    private val addDefaultAschariteScript = script { src = "/ascharite/common.js" }
    private val sockJsScript = script { src = "/webjars/sockjs-client/sockjs.min.js" }
    private val stompJsScript = script { src = "/webjars/stomp-websocket/stomp.min.js" }

    override fun render(model: MutableMap<String, *>?, request: HttpServletRequest, response: HttpServletResponse) {
        addDefaultAschariteScript(model!!["html"] as Html)
        response.contentType = "text/html;charset=UTF-8"
        val writer = response.writer
        writer.write(model["html"].toString())
    }

    private fun addDefaultAschariteScript(html: Html) {
        var headTag = html.children.find { it::class == Head::class }
        if (headTag == null) {
            headTag = head {
                +sockJsScript
                +stompJsScript
                +addDefaultAschariteScript
            }
            html.children.add(0, headTag)
        } else {
            (headTag as HtmlTag).children.add(0, sockJsScript)
            headTag.children.add(1, stompJsScript)
            headTag.children.add(2, addDefaultAschariteScript)
        }
    }

    override fun getContentType(): String? {
        return "text/html"
    }
}