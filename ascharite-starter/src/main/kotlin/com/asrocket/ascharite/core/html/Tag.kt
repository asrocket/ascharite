package com.asrocket.ascharite.core.html

@HtmlTagMarker
abstract class Tag(val name: String) : Element {
    val children = arrayListOf<Element>()
    val attributes = hashMapOf<String, String>()

    protected fun <T : Element> initTag(tag: T, init: T.() -> Unit): T {
        tag.init()
        children.add(tag)
        return tag
    }

    override fun render(builder: StringBuilder, indent: String, prettify: Boolean) {
        if (prettify) {
            builder.append("$indent<$name${renderAttributes()}>\n")
            for (c in children) {
                c.render(builder, "  $indent", prettify)
            }
            builder.append("$indent</$name>\n")
        } else {
            builder.append("$indent<$name${renderAttributes()}>")
            for (c in children) {
                c.render(builder, "$indent", prettify)
            }
            builder.append("$indent</$name>")
        }
    }

    protected fun renderAttributes(): String {
        val builder = StringBuilder()
        for ((attr, value) in attributes) {
            builder.append(" $attr=\"$value\"")
        }
        return builder.toString()
    }

    override fun toString(): String {
        val builder = StringBuilder()
        render(builder, "", false)
        return builder.toString()
    }

    fun toStringPrettify(): String {
        val builder = StringBuilder()
        render(builder, "", true)
        return builder.toString()
    }

    operator fun String.unaryPlus() {
        children.add(TextElement(this))
    }

    operator fun Element.unaryPlus() {
        children.add(this)
    }
}