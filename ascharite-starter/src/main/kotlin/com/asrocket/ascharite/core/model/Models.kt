package com.asrocket.ascharite.core.model

open class Event(val type: String, val data: Any?)

class ElementEvent(type: String, val id: String, data: Any) : Event(type, data)