package com.asrocket.ascharite.core.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(WebConfiguration::class, WebSocketConfiguration::class)
class AschariteAutoConfiguration