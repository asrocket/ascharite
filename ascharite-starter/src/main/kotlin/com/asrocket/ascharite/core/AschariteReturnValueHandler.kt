package com.asrocket.ascharite.core

import com.asrocket.ascharite.core.html.Html
import org.springframework.core.MethodParameter
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.support.HandlerMethodReturnValueHandler
import org.springframework.web.method.support.ModelAndViewContainer

class AschariteReturnValueHandler : HandlerMethodReturnValueHandler {

    override fun supportsReturnType(returnType: MethodParameter) = returnType.method!!.returnType == Html::class.java

    override fun handleReturnValue(returnValue: Any?, returnType: MethodParameter, mavContainer: ModelAndViewContainer, webRequest: NativeWebRequest) {
        mavContainer.viewName = "defaultAschariteView"
        mavContainer.addAttribute("html", returnValue)
    }
}