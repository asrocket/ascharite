package com.asrocket.ascharite.core.html

var HtmlTag.onClick: String
    get() = attributes["onclick"] ?: ""
    set(value) {
        attributes["onclick"] = value
    }

var HtmlTag.onDblclick: String
    get() = attributes["ondblclick"] ?: ""
    set(value) {
        attributes["ondblclick"] = value
    }

var HtmlTag.onMousedown: String
    get() = attributes["onmousedown"] ?: ""
    set(value) {
        attributes["onmousedown"] = value
    }

var HtmlTag.onMousemove: String
    get() = attributes["onmousemove"] ?: ""
    set(value) {
        attributes["onmousemove"] = value
    }

var HtmlTag.onMouseout: String
    get() = attributes["onmouseout"] ?: ""
    set(value) {
        attributes["onmouseout"] = value
    }

var HtmlTag.onMouseover: String
    get() = attributes["onmouseover"] ?: ""
    set(value) {
        attributes["onmouseover"] = value
    }

var HtmlTag.onMouseup: String
    get() = attributes["onmouseup"] ?: ""
    set(value) {
        attributes["onmouseup"] = value
    }