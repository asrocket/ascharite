package com.asrocket.ascharite.core.html

interface Element {
    fun render(builder: StringBuilder, indent: String, prettify: Boolean)
}