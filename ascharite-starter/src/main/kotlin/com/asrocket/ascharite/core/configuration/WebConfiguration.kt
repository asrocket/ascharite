package com.asrocket.ascharite.core.configuration

import com.asrocket.ascharite.core.AschariteReturnValueHandler
import com.asrocket.ascharite.core.AschariteView
import org.springframework.context.annotation.Bean
import org.springframework.web.method.support.HandlerMethodReturnValueHandler
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

class WebConfiguration : WebMvcConfigurer {

    @Bean
    fun defaultAschariteView() = AschariteView()

    override fun addReturnValueHandlers(handlers: MutableList<HandlerMethodReturnValueHandler>) {
        handlers.add(AschariteReturnValueHandler())
    }

    override fun configureViewResolvers(registry: ViewResolverRegistry) {
        registry.beanName()
    }

}