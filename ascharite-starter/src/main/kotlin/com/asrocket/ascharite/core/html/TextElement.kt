package com.asrocket.ascharite.core.html

class TextElement(val text: String) : com.asrocket.ascharite.core.html.Element {
    override fun render(builder: StringBuilder, indent: String, prettify: Boolean) {
        if (prettify) {
            builder.append("$indent$text\n")
        } else {
            builder.append("$indent$text")
        }
    }
}
